cmake_minimum_required(VERSION 3.12.0)
project(shogiserver VERSION 1.0 LANGUAGES CXX)

set(CMAKE_VERBOSE_MAKEFILE OFF) # See makefile output
set(debug ON)                   # Turn on debug settings
set(logging ON)                 #Log messages

set(compileOptions -pipe -O2 -Wall -Wextra -march=native -std=c++20)    # set compile option
# -O3 -ffast-math -funroll-loops -funswitch-loops?

#Qt
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)   #export compile commands for tools like rtags
link_directories(SYSTEM "/usr/include/qt5/")


# set complete compile flags depending on the compiler
if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  list(APPEND compileOptions -Wdeprecated -Weverything -Wno-vla-extension -Wno-vla -Wno-padded -Wno-float-equal -Wno-used-but-marked-unused -Wno-documentation-unknown-command -Wno-weak-vtables -Wno-format-nonliteral -Wno-c99-extensions -Wno-c++98-compat -Wno-c++98-compat-pedantic -Wno-global-constructors -Wno-exit-time-destructors)
elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  list(APPEND compileOptions -Wall -W -Wconversion -Wshadow -Wpointer-arith -Wcast-qual -Wcast-align -Wwrite-strings -fno-common)
else()
  list(APPEND compileOptions -Wall -W -Wconversion -Wshadow -Wpointer-arith -Wcast-qual -Wcast-align -Wwrite-strings -fno-common)
  message("unknown compiler!")
endif()

if(CMAKE_INSTALL_PREFIX)
  set(prefix ${CMAKE_INSTALL_PREFIX})
elseif(ENV{PREFIX})
  set(prefix $ENV{PREFIX})
else()
  set(prefix "/usr/local")
endif()
message("prefix set as ${prefix}.")

set(resourcesDirectory "${prefix}/share/${PROJECT_NAME}")
set(binaryDirectory "${prefix}/bin/${PROJECT_NAME}")
set(libraryDirectory "${prefix}/lib/${PROJECT_NAME}")
set(headersDirectory "${prefix}/include/${PROJECT_NAME}")
set(licenseDirectory "${resourcesDirectory}/license/")

# pass some info as preprocessor definitions
set(definitions
  cmake_MAJOR=${PROJECT_VERSION_MAJOR}
  cmake_MINOR=${PROJECT_VERSION_MINOR}
  cmake_PROJECT_NAME="${PROJECT_NAME}"
  cmake_RESOURCES_DIRECTORY="${resourcesDirectory}"
  cmake_BINARY_DIRECTORY="${binaryDirectory}"
  cmake_LIBRARY_DIRECTORY="${libraryDirectory}"
  cmake_HEADERS_DIRECTORY="${headersDirectory}"
  cmake_LICENSE_DIRECTORY="${licenseDirectory}")

# set other compile flags and preprocessor directives depending on enabled options
if(${logging})
  list(APPEND definitions "_Logging")
endif()
if(${debug})
  list(APPEND definitions ___Debug)
  list(APPEND compileOptions -g3)
else()
  list(APPEND definitions NDEBUG)
endif(${debug})

# Actually set glabal parameters
add_compile_definitions(${definitions})
add_compile_options(${compileOptions})
include_directories(SYSTEM "/usr/include/qt/")
include_directories(SYSTEM "/usr/include/qt5/")

# Setup all components: libraries, clients, ...
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/server)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/icons/piece_iconsets)

# Documentation
find_package(Doxygen)
if(${DOXYGEN_FOUND})
  message(WARNING "Doxygen documentation generation is currently not functional.")
  # add_custom_target( doc_doxygen ALL
  #       COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
  #       WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  #       COMMENT "Generating API documentation with Doxygen"
  #       VERBATIM )
  # doxygen_add_docs("doc"
  #   ${CMAKE_CURRENT_SOURCE_DIR}
  #   [WORKING_DIRECTORY dir]
  #   [COMMENT comment])
else()
  message(WARNING "Doxygen not found, skipping the target for documentation generation")
endif()

# install license
install(FILES "COPYING.LESSER" DESTINATION "${licenseDirectory}" PERMISSIONS WORLD_READ)
install(FILES "COPYING" DESTINATION "${licenseDirectory}" PERMISSIONS WORLD_READ)
