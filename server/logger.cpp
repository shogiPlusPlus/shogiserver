/***************************************************************************
 * Copyright (C) 2021 Francesco Florian
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "logger.h"

#include <array>
#include <cstdio>
#include <string>

namespace ShogiServer {
  namespace Logger {
    namespace{
      static std::array<std::string const, static_cast<size_t>(Color::none)+1> const colorStrings = {ANSI_COLOR_BLUE, ANSI_COLOR_CYAN, ANSI_COLOR_GREEN, ANSI_COLOR_MAGENTA, ANSI_COLOR_RED, ANSI_COLOR_YELLOW, ANSI_COLOR_DEFAULT};
      inline std::string colorString(Color const color) {return colorStrings[static_cast<size_t>(color)];}

      NormalLevel logLevel_ = NormalLevel::max;
      WarnLevel warnLevel_ = WarnLevel::max;
      LogDevice *globalLogger = nullptr;
    }
    LogDevice::LogDevice() {
      if(globalLogger != nullptr) {
        warn("A logger is already registered as global logger, not overwriting!", WarnLevel::error, Color::yellow);
        return;
      }
      globalLogger = this;
    }
    LogDevice::~LogDevice() {
      if(globalLogger != this) {
        warn("This object is not the registered global logger, not resetting!", WarnLevel::error, Color::yellow);
        return;
      }
      globalLogger = nullptr;
    }

    void LogDevice::addDevice() const {}
    void LogDevice::write(std::string const message) const {
      printf("%s\n", message.c_str());
    }

    void log(std::string const message, NormalLevel const type, Color const color) {
      if(globalLogger == nullptr)
        return;
      if(type < logLevel_)
        globalLogger->write(colorString(color) + message + colorString(Color::none));
    }
    void warn(std::string const message, WarnLevel const type, Color const color) {
      if(globalLogger == nullptr)
        return;
      if(type < warnLevel_)
        globalLogger->write(colorString(color) + message + colorString(Color::none));
    }
    NormalLevel logLevel() { return logLevel_;}
    NormalLevel logLevel(NormalLevel const severity) {
      logLevel_ = severity;
      return logLevel();
    }
    WarnLevel warnLevel() { return warnLevel_; }
    WarnLevel warnLevel(WarnLevel const severity) {
      warnLevel_ = severity;
      return warnLevel();
    }
  }
}
