/***************************************************************************
 * Copyright (C) 2021 Francesco Florian, Élise Le Mélédo
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef BOARD_H
#define BOARD_H

#include <array>
#include <span>
#include <unordered_set>

#include <QtCore/QObject>

#include "piece.h"

namespace ShogiServer {
  /**
   * A representation of the board with contextual validity check of the moves.
   */
  class Board
    : public QObject
  {
    Q_OBJECT
  public:
    static constexpr size_t rows_ = 9;   //!< Number of rows.
    static constexpr size_t columns_ = 9;//!< Number of columns.
    /**
     * An identifier for a player based on its role in the game.
     */
    enum class PlayerId {
      first,    //!< First player.
      second,   //!< Second player.
      none      //!< No player (e.g., for an empty cell).
    };

    using Cell = std::pair<Piece *, PlayerId>;  //!< Type describing a cell. Includes a pointer to the Piece which is present on that cell and information on the owner.
    using DropList = std::unordered_multiset<Piece::Type>;
    static const DropList emptyDropList;
    static constexpr Cell emptyCell {nullptr, PlayerId::none};  //!< Constant describing an empty Cell.

    static Position reversePlayer(Position const position) { return {static_cast<int>(rows_) - x(position) - 1, static_cast<int>(columns_) - y(position) - 1};}
    static Position reversePlayer(size_t const row, size_t const column) { return reversePlayer({static_cast<int>(row), static_cast<int>(column)}); }
    static bool isPromotionArea(Position const position, PlayerId player) {
      switch(player) {
      case PlayerId::none:
        return false;
      default: // Simplified here as the board is turned anyway
        return (y(position) > 5) && (y(position) < static_cast<int>(rows_));
      }
    }

    Board();
    /**
     * Access the cell at the given position.
     *
     * \return Cell at position
     */
    Cell& at(size_t const row,  //!< Target row
             size_t const column//!< Target column
             );
    /**
     * Access the cell at the given position.
     *
     * \return Cell at position
     */
    Cell& at(Position const position    //!< Target position
             ) {
      if(x(position) < 0 || y(position) < 0)
        return at(rows_ + 1, columns_ + 1);
      return at(static_cast<size_t>(x(position)), static_cast<size_t>(y(position)));
    }
    /**
     * Access the cell at the given position.
     *
     * \return Cell at position
     */
    Cell& operator[](Position const position    //!< Target position
                     ) { return at(position); }

    /**
     * Drop a piece.
     *
     * Since the board is aware of the position of the other pieces, this function is able to perform a complete check that the move is legal before performing it.
     * The player dropping the piece is assumed to be the one whose id is stored in turnCounter_.
     *
     * @return Whether the operation was successful.
     */
    bool drop(Piece const * const piece,//!< Piece to drop
              Position const position   //!< Target position
              );
    /**
     * Drop a piece.
     *
     * Since the board is aware of the position of the other pieces, this function is able to perform a complete check that the move is legal before performing it.
     * The player moving the piece is assumed to be the one whose id is stored in turnCounter_.
     * The move will fail if the position of the piece is not on the board.
     * The move will fail il the piece is owned by a different player (ownership is checked based on the position).
     *
     * @return Whether the operation was successful.
     */
    bool move(Piece * const piece,//!< Piece to move
              Position const position   //!< Target position
              );


    // Helper functions
    bool TwoDIntVectorMembership(std::vector<std::array<int,2>> FullVect,  //!< a 2D Nx2 vector for which the rows will be compared with a int array of size 2
                                 std::array<int,2> VectElement             //!< a int array of size 2 to be looked for in the 2D vector
                                 );                                          //!< Checks membership of a integer row in a 2D vector \return a bool

    // Game logic routines
    bool PieceIsChecking(Piece const * const piece,  //!< The piece of interest
                        Position const position     //!< The target position of the piece
                        );                            //!< Checks if the given piece checks the opponent king

    bool ValidMove(Piece const * const piece,       //!< The piece one wants to move
                    Position const position          //!< The target move position
                    );                               //!< Check if a given move is valid

    bool ValidDrop(Piece const * const piece,       //!< The piece one wants to drop
                    Position const position          //!< The target drop position
                    );                               // Check if a given drop is valid

    // Routines used for checking if one is checked
    Position GetKingPosition(PlayerId Player        //!< The considered playerId
                            );                     //!< Retrieving the king position of the given player

    /**
    // Function determining the attack range of the WhoseAttacking player (assuming the turn of turnCounter_)
    The function considers that it is the turnCounter_'s turn, and allows the possibility to check the attack span of the player
    or the opponent, either with the actual piece layout (only WhoseAttacking argument), in case of the layout that would become
    after a drop of the current player (only WhoseAttacking argument and NewPiece argument), or in case of the layout that would
    become after a move of the current player (three arguments case):

    If no optional argument is given, the actual board is considered.
    If one optional arguments are given, the active player piece is dropped to the given position
    If both optional arguments are given, the active player piece is moved to the given position

    TODO: Make robust assert for arguments
    \return the set of spots where the WhoseAttacking player can go (in its globality, regarless the moving pieces themselves). */
    std::vector<std::array<int, 2>> AttackRange(PlayerId WhoseAttacking,    //!< The playerId for which to consider the attacked zone
                                                Piece const NewPiece,       //!< (optional, mandatory is OmittedPiece is given) A temporary new piece to consider
                                                Piece const OmmitedPiece);  //!< (optional) A temporary existing piece to omit


    // Pieces subset routines
    std::vector<Piece> GetPiecesSet(PlayerId WhosePieces,     //!< The playerId to retrieve the piece for
                                    Piece const NewPiece,     //!< (optional, mandatory is OmittedPiece is given) A temporary new piece to consider
                                    Piece const OmmitedPiece  //!< (optional) A temporary existing piece to omit
                                    ); //!< Retrieve the set of the selected player's pieces

    /**
    Function determining the defended spots by the WhoseProtectedSpots player (assuming the turn of turnCounter_),
    extracts the spots that are protected by the pieces of WhoseProtectedSpots.
    The function considers that it is the turnCounter_'s turn, and allows the possibility to check the protected span of the player
    or the opponent, either with the actual piece layout (only WhoseProtectedSpots argument), in case of the layout that would become
    after a drop of the current player (only WhoseProtectedSpots argument and NewPiece argument), or in case of the layout that would
    become after a move of the current player (three arguments case):

    If no optional argument is given, the actual board is considered.
    If one optional arguments are given, the active player piece is dropped to the given position
    If both optional arguments are given, the active player piece is moved to the given position
    The same logic applied for the last arguments, where it would be a 2-step-anticipated board

    \return Returns the set of spots that are protected by the chosen player (in its globality, regarless the protecting piece itself).*/
    std::vector<std::array<int, 2>> ProtectedSpots(PlayerId WhoseProtectedSpots,       //!< The playerId for which to consider the protected zone
                                                    Piece const NewPiece,               //!< (optional, mandatory is OmittedPiece is given) A temporary new piece to consider, belonging to the WhoseProtectedSpots's opponent
                                                    Piece const OmmitedPiece,           //!< (optional) A temporary new piece to consider, belonging to the WhoseProtectedSpots's opponent
                                                    Piece const ResponsePiece,          //!< (optional, mandatory is OmittedPiece is given) A temporary new piece to consider at t+2 time, belonging to the WhoseProtectedSpots
                                                    Piece const OmmitedResponsePiece    //!< (optional) A temporary new piece to consider at t+2 time, belonging to the WhoseProtectedSpots
                                                    );

    // Moves subsets routines
    std::vector<std::array<int, 2>> TheoreticalPhysicalMoves(Piece const * const piece,   //!< Considered piece
                                                            Position const InitPosition  //!< (optional) If one wants to check for another position for the piece, the target position
                                                            );                            //!< Gives all the range of moves regardless the board layout

    std::vector<std::array<int, 2>> PhysicalMoves(Piece const * const piece,              //!< Considered piece
                                                Position const  InitPosition            //!< (optional) If one wants to check for another position for the piece, the target position
                                                );                                      //!< Gives all the range of moves considering the board layout and other pieces

    std::vector<std::array<int, 2>> ContextualMoves(Piece const * const piece             //!< Considered piece
                                                    );                                    //!<  \return the set of all spots where to the piece can be safely moved by the current player

    std::vector<std::array<int, 2>> ContextualDrops(Piece const * const piece             //!< Considered piece
                                                    );                                    //!< \return the set of all spots where the piece can be safely droped by the current player

    bool Check(PlayerId consideredPlayer                                                  //!< The player to check the check mate status for
    );                                                                                    //!< \return whether the considered player is check mate

    bool CheckMate(PlayerId consideredPlayer                                              //!< The player to check the check status for
    );                                                                                    //!< \return whether the considered player is check mate

    bool CloseTurn();                                                                     //!< Function closing the turn and flipping the board/pieces indices

    PlayerId GetTurn(){return turnCounter_;}                                              //!< Function returning whose turn it is

  signals:
    void pieceEaten(Piece &);
    void enteredPromotionArea(Piece &);
    void check();
    void gameWin();
    void gameLost();

  protected:
    void advanceTurn(); //!< Advance the turn changing the tournCounter that describes the current player.
    bool fuyoPresent(size_t const column        //!< The column where the player wants to drop something
                     );                          //!< Check if there is already one player's pon in the column

    bool canDrop(PlayerId const player, //!< Player wishing to drop.
                 Piece::Type const piece//!< Type of the piece that would be dropped.
                 ) const;               //!< Check if the player is allowed to drop a specific piece. \return wether the piece was found in the droplist.
    bool doDrop(PlayerId const player, //!< Player wishing to drop.
                 Piece::Type const piece//!< Type of the piece that would be dropped.
                 );                     //!< Check if the player is allowed to drop a specific piece, and if so remove it. \return wether the piece was found in the droplist.
    void addToDropList(PlayerId const player,   //!< Player who got the piece.
                       Piece::Type const piece  //!< Type of the new piece.
                       );                       //!< Add a new piece to the Player droplist.

    PlayerId turnCounter_ = PlayerId::first;
    PlayerId tocheck_     = PlayerId::second;

    std::array<Cell, rows_ * columns_> board_;  //!< All of the board Cells. The arrangment is column-major, but that should not be important as it is managed by at.
    std::array<DropList,2> dropLists_;
  };
}

#endif /* BOARD_H */
