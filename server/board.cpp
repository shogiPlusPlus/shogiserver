/***************************************************************************
 * Copyright (C) 2021 Francesco Florian, Élise Le Mélédo
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#include "board.h"

#include <array>
#include <span>
#include <algorithm>

#include<iostream>
#include "piece.h"

namespace ShogiServer {
  static constexpr std::array<std::array<Piece::Type,Board::columns_>,3> initialDisposition = {
    std::array<Piece::Type,Board::columns_>{Piece::Type::kyosha, Piece::Type::keima, Piece::Type::gin, Piece::Type::kin, Piece::Type::osho, Piece::Type::kin, Piece::Type::gin, Piece::Type::keima, Piece::Type::kyosha},
    std::array<Piece::Type,Board::columns_>{Piece::Type::none, Piece::Type::kakugyo, Piece::Type::none, Piece::Type::none, Piece::Type::none, Piece::Type::none, Piece::Type::none, Piece::Type::hisha, Piece::Type::none},
    std::array<Piece::Type,Board::columns_>{Piece::Type::fuyo, Piece::Type::fuyo, Piece::Type::fuyo, Piece::Type::fuyo, Piece::Type::fuyo, Piece::Type::fuyo, Piece::Type::fuyo, Piece::Type::fuyo, Piece::Type::fuyo}
  };

  Board::Board()
    :dropLists_{emptyDropList, emptyDropList} {
    board_.fill({nullptr, PlayerId::none});
    for(unsigned int row = 0; row < initialDisposition.size(); ++row)
      for(unsigned int column = 0; column < initialDisposition[0].size(); ++column)
        if(initialDisposition[row][column] != Piece::Type::none) {
          Piece *tmp = new Piece(initialDisposition[row][column], column, row);
          Piece *tmp2 = new Piece(initialDisposition[row][column], columns_-1-column, rows_-1-row);
          at(row, column) = {tmp, PlayerId::first};
          at(reversePlayer(row, column)) = {tmp2, PlayerId::second};
        }
    connect(this, &Board::pieceEaten, this, [this](Piece const &piece){ addToDropList(turnCounter_, piece.type()); });
  }

  Board::Cell& Board::at(size_t const row, size_t const column) {
    if((row >= rows_) || (column >= columns_)) {
      Cell *tmp = new Cell(emptyCell);
      return *tmp;
    }
    return board_[column * rows_ + row];
  }

  // ------------------------------------- C++ helper functions ----------------------------------------------------------
  // Simple function checking row membership in Nx2 double array
  bool Board::TwoDIntVectorMembership(std::vector<std::array<int,2>> FullVect,Position VectElement){
    bool flag = false;
    for (auto &vecele : FullVect) {if (std::equal(std::begin(vecele), std::end(vecele), std::begin(VectElement))) {flag = true; break;}}
    return (flag);
  }

  // Function telling if the considered piece is checking the opponent
  bool Board::PieceIsChecking(Piece const * const piece, Position const position){
    int maxx = Board::columns_;
    int maxy = Board::rows_;
    int a = std::max({abs(maxx-x(position)), abs(maxy-y(position)), abs(maxx+x(position)), abs(maxy+y(position))});

    for(auto &move : piece->moves()){
      if(move.second) {
        for(int idx = 1; idx <= a; idx++) {
          int Posx = move.first[0] * idx + x(position);
          int Posy = move.first[1] * idx + y(position);

          auto Spot = at(static_cast<size_t>(Posy), static_cast<size_t>(Posx));
          if (Spot.first) {
            if(Spot.first-> type() == Piece::Type::osho && Spot.second==tocheck_)
              return true;
            break;
          }
        }
      } else {
        int Posx = move.first[0] + x(position);
        int Posy = move.first[1] + y(position);
        auto Spot = at(static_cast<size_t>(Posy), static_cast<size_t>(Posx));
        if (Spot.first && Spot.first-> type() == Piece::Type::osho)
          return true;
      }
    }
    return false;
  }

  // ------------------------------------- Shogi helper functions ----------------------------------------------------------
  // Function telling if the considered piece is checking the opponent
  Position Board::GetKingPosition(PlayerId Player){
    // Initialising the position to return undeployed if the king was not found
    Position Pos = {-1,-1};

    // Looking for the king
    for(unsigned int idx = 0; idx < Board::columns_; idx++) {
      for(unsigned int idy = 0; idy < Board::columns_; idy++) {
        auto Spot = at(static_cast<size_t>(idy), static_cast<size_t>(idx));
        if (Spot.first && Spot.first->type()== Piece::Type::osho && Spot.second == Player) {
          Pos = Spot.first->position();
          break;
        }
      }
    }
    return (Pos);
  }

  // Extract the pieces set from the board acoording to the considered player
  std::vector<Piece> Board::GetPiecesSet(PlayerId WhosePieces, Piece const NewPiece = Piece::Type::none, Piece const OmmitedPiece = Piece::Type::none){
    // Extract the pieces set from the board acoording to the considered player
    // The omit position and new piece are there to anticipate a possible piece move/drop without having to modify/deep copy the board

    // Initialisations
    std::vector<Piece> PiecesSet;       // Initialising the storing vector of pieces
    Position OmitPosition = {-1,-1};    // Initialising the omit position to no position to omit

    // Checking the cases according to the inputs
    if      (NewPiece.type()!=Piece::Type::none  && OmmitedPiece.type()!=Piece::Type::none) {OmitPosition = OmmitedPiece.position(); PiecesSet.push_back(NewPiece);}
    else if (NewPiece.type()!=Piece::Type::none) {PiecesSet.push_back(NewPiece);}

    for(unsigned int idx = 0; idx < Board::columns_; idx++) {
      for(unsigned int idy = 0; idy < Board::rows_; idy++) {
        // Retrieving the cell of the board
        auto Spot = at(static_cast<size_t>(idy), static_cast<size_t>(idx));

        // Checking if there is a piece there and if yes, gather it
        if (Spot.first && Spot.second==WhosePieces && Spot.first->position()!=OmitPosition) {
          Piece PieceCopy = Piece(Spot.first->type(),idx,idy);
          PiecesSet.push_back(PieceCopy);
        }
      }
    }
    return (PiecesSet);
  }

  // Function determining the defended spots by the WhoseProtectedSpots player (assuming the turn of turnCounter_)
  std::vector<std::array<int, 2>> Board::ProtectedSpots(PlayerId WhoseProtectedSpots, Piece const NewPiece = Piece::Type::none, Piece const OmmitedPiece = Piece::Type::none, Piece const ResponsePiece = Piece::Type::none, Piece const OmmitedResponsePiece=Piece::Type::none){
    // Returns the set of spots that are protected by the chosen player (in its globality, regarless the protecting piece itself).
    // The function considers that it is the turnCounter_'s turn, and allows the possibility to check the protected span of the player
    // or the opponent, either with the actual piece layout (only WhoseProtectedSpots argument), in case of the layout that would become
    // after a drop of the current player (only WhoseProtectedSpots argument and NewPiece argument), or in case of the layout that would
    // become after a move of the current player (three arguments case):

    // If no optional argument is given, the actual board is considered.
    // If one optional arguments are given, the active player piece is dropped to the given position
    // If both optional arguments are given, the active player piece is moved to the given position
    // The same logic applied for the last arguments, where it would be a 2-step-anticipated board

    // --------------------- Precomputations -------------------------------------
    // Declarations
    std::vector<std::array<int,2>> AttackingPositions;      // The table containing the actual positions of deployed pieces of the player (NbDeployedPieces x (row, column))
    std::vector<std::array<int,2>> ProtectingPositions;     // The table containing the actual positions of deployed pieces of the other player (NbDeployedPieces x (row, column))
    std::vector<std::array<int,2>> ProtectedSpotsList;      // The table containing the protected spots (NbMoves x (row, column))
    std::vector<Piece> ProtectingPieces;                    // The containor of the pieces of the playing player
    std::vector<Piece> AttackingPieces;                     // The containor of the pieces of the other player
    std::array<int, 2> BufferSpot;                          // Buffer variable
    int span=1;                                             // Buffer variable
    int MovesDirection=1;                                   // Indicator of which direction to go for

    // Initialisations
    auto OpponentPieces     = GetPiecesSet(tocheck_,ResponsePiece,OmmitedResponsePiece);      // Retrieving all the opponent pieces
    auto ActivePlayerPieces = GetPiecesSet(turnCounter_,NewPiece, OmmitedPiece);              // Getting the player piece including the possibly anticipated modification

    // Mapping towards the considerd protecting/attacking side wished by the user
    if (WhoseProtectedSpots == tocheck_) {
      ProtectingPieces = OpponentPieces;
      AttackingPieces  = ActivePlayerPieces;
      MovesDirection   = -1;
    } else if (WhoseProtectedSpots == turnCounter_) {
      ProtectingPieces = ActivePlayerPieces;
      AttackingPieces  = OpponentPieces;
      MovesDirection   = 1;
    } else {
      //raise error
    }

    // --------------------- Checking all the range of proteced spot -------------------------------------
    // Extracting the positions of all the player pieces
    for (auto &piece: AttackingPieces)  {AttackingPositions.push_back(piece.position());}
    for (auto &piece: ProtectingPieces) {ProtectingPositions.push_back(piece.position());}

    // --------------------- Checking all the range of proteced spot -------------------------------------
    // Getting the cumulated span of their action according to the board layout and other deployed pieces
    for (auto &piece: ProtectingPieces) {
      // Get the position of all the protecting pieces
      Position Pos = piece.position();

      // Determining the range of action of long-distance pieces
      int maxx = static_cast<int>(Board::columns_);
      int maxy = static_cast<int>(Board::rows_);
      int maxspan = std::max({(maxx-x(Pos)), (maxy-y(Pos)), (maxx+x(Pos)), (maxy+y(Pos))});

      // Checking the validity of the protected spots per piece (against blocked span from other pieces etc)
      // Only checking the protection, not the ability to actually move (not considering self-checking repercusions)
      for(auto &move: piece.moves()) {
        // Select the span depending on whether the type of move is a long-distance one or a precise location spot
        if  (move.second) {span = static_cast<int>(maxspan);}
        else              {span = 1;}

        // Loop over the  span of the piece
        for(int idx = 1; idx <= span; idx++) {
          // Compute the theoretical reachable position of the piece (considering if opponent or actualplayer)
          int Posx = MovesDirection*move.first[0] * idx + x(Pos);
          int Posy = MovesDirection*move.first[1] * idx + y(Pos);

          // Considering the spot if it falls within the boards limits
          if((Posx >= 0) && (Posx <= static_cast<int>(Board::columns_)) && (Posy >=0) && (Posy<static_cast<int>(Board::rows_))) {
            // Considering the spot as a protectedspot
            BufferSpot = {Posx, Posy};

            // Adding in the list if not already there
            if (!TwoDIntVectorMembership(ProtectedSpotsList,{BufferSpot})) {ProtectedSpotsList.push_back({BufferSpot});}

            // Breaking the span if the spot is occupied
            if (TwoDIntVectorMembership(AttackingPositions,{BufferSpot})||TwoDIntVectorMembership(ProtectingPositions,{BufferSpot})) {break;}
          }
        }
      }
    }

    // Returning the protected spots
    return (ProtectedSpotsList);
  }

  // Function determining the attack range of the WhoseAttacking player (assuming the turn of turnCounter_)
  std::vector<std::array<int, 2>> Board::AttackRange(PlayerId WhoseAttacking, Piece const NewPiece = Piece::Type::none, Piece const OmmitedPiece = Piece::Type::none){
    // Returns the set of spots where the WhoseAttacking player can go (in its globality, regarless the moving pieces themselves).
    // The function considers that it is the turnCounter_'s turn, and allows the possibility to check the attack span of the player
    // or the opponent, either with the actual piece layout (only WhoseAttacking argument), in case of the layout that would become
    // after a drop of the current player (only WhoseAttacking argument and NewPiece argument), or in case of the layout that would
    // become after a move of the current player (three arguments case):

    // If no optional argument is given, the actual board is considered.
    // If one optional arguments are given, the active player piece is dropped to the given position
    // If both optional arguments are given, the active player piece is moved to the given position

    // TODO: Make robust assert for arguments
    //!< \todo check the belongings and theright execution


    // --------------------- Precomputations -------------------------------------
    // Declarations
    std::vector<std::array<int,2>> AttackSpotList;          // The table containing the attack spots (NbMoves x (row, column))
    std::vector<std::array<int,2>> AttackingPositions;      // The table containing the actual positions of deployed pieces of the player (NbDeployedPieces x (row, column))
    std::vector<std::array<int,2>> ProtectingPositions;     // The table containing the actual positions of deployed pieces of the other player (NbDeployedPieces x (row, column))
    std::vector<Piece> ProtectingPieces;                    // The containor of the pieces of the playing player
    std::vector<Piece> AttackingPieces;                     // The containor of the pieces of the other player
    std::array<int, 2>             BufferMove;              // Buffer variable
    int span = 1;                                           // Buffer variable
    PlayerId otherplayer = PlayerId::second;                // Indicator of the other player
    Position King = {-1,-1};                                // King position
    int MovesDirection=1;                                   // Indicator of which direction to go for

    // Initialisations
    auto OpponentPieces     = GetPiecesSet(tocheck_);                                     // Retrieving all the opponent pieces
    auto ActivePlayerPieces = GetPiecesSet(turnCounter_,NewPiece, OmmitedPiece);           // Getting the player piece including the possibly anticipated modification

    // Mapping towards the considerd protecting/attacking side wished by the user
    if (WhoseAttacking == turnCounter_) {
      otherplayer      = turnCounter_;
      ProtectingPieces = OpponentPieces;
      AttackingPieces  = ActivePlayerPieces;
      MovesDirection    = -1;
    } else if (WhoseAttacking == tocheck_) {
      otherplayer      = tocheck_;
      ProtectingPieces = ActivePlayerPieces;
      AttackingPieces  = OpponentPieces;
      MovesDirection    = 1;
    } else {
      //raise error
    }

    // --------------------- Checking all the range of proteced spot -------------------------------------
    // Extracting the positions of all the player pieces
    for (auto &piece: AttackingPieces)  {AttackingPositions.push_back(piece.position());}
    for (auto &piece: ProtectingPieces) {ProtectingPositions.push_back(piece.position());}

    // Getting the cumulated span of their action according to the board layout and other deployed pieces
    for (auto &piece: AttackingPieces) {
      // Get the position of all the protecting pieces
      Position Pos = piece.position();

      // Determining the range of action of long-distance pieces
      int maxx = Board::columns_;
      int maxy = Board::rows_;
      int maxspan = std::max({(maxx-x(Pos)), (maxy-y(Pos)), (maxx+x(Pos)), (maxy+y(Pos))});

      // Checking the validity of the protected spots per piece (against blocked span from other pieces etc)
      // Only checking the protection, not the ability to actually move (not considering self-checking repercusions)
      for(auto &move: piece.moves()) {
        // Select the span depending on whether the type of move is a long-distance one or a precise location spot
        if  (move.second) {span = maxspan;} else {span = 1;}

        // Loop over the long-distance span of the piece
        for(int idx = 1; idx <= span; idx++) {
          // Compute the theoretical reachable position of the piece (considering if opponent or actualplayer)
          int Posx = MovesDirection*move.first[0]*idx + x(Pos);
          int Posy = MovesDirection*move.first[1]*idx + y(Pos);

          // Considering the spot if it falls within the boards limits
          if((Posx >= 0) && (Posx <= static_cast<int>(Board::columns_)) && (Posy >=0) && (Posy<static_cast<int>(Board::rows_))) {
            // Considering the spot as an attacking spot
            BufferMove = {Posx, Posy};

            // Considering the pieces to move anticipatidely
            Piece ResponsePiece        = Piece(piece.type(),static_cast<unsigned int>(x(Pos)),static_cast<unsigned int>(y(Pos)));
            Piece OmmitedResponsePiece = Piece(piece.type(),static_cast<unsigned int>(Posx),static_cast<unsigned int>(Posy));

            // One can move there if (the spot is free, or the piece there belongs to the other player -> the spot is not self-occupied already)
            bool CanEat = !TwoDIntVectorMembership(AttackingPositions,{BufferMove});

            // Further check: one can move there if moving the piece does not make the player checked
            // Check with the possibly anticipated layout (moved/dropped NewPiece) of the other player
            if   (OmmitedPiece.type() == Piece::Type::osho) {King = NewPiece.position();}
            else                                            {King = GetKingPosition(WhoseAttacking);}
            auto OtherPlayerProtectedSpots = ProtectedSpots(otherplayer, NewPiece, OmmitedPiece, ResponsePiece, OmmitedResponsePiece);
            bool Checked = TwoDIntVectorMembership(OtherPlayerProtectedSpots,King);

            // Adding in the list if not already there
            if (!Checked && CanEat && !TwoDIntVectorMembership(AttackSpotList,{BufferMove})) {AttackSpotList.push_back({BufferMove});}

            // Breaking the span if the spot is occupied
            if (TwoDIntVectorMembership(AttackingPositions,{BufferMove})||TwoDIntVectorMembership(ProtectingPositions,{BufferMove})) {break;}
          }
        }
      }
    }

    // Returning the protected spots
    return (AttackSpotList);
  }

  // ------------------------------------- Board helper functions ----------------------------------------------------------
  // Swapping the board player information
  void Board::advanceTurn(){
    std::swap(turnCounter_,tocheck_);
  }

  // Checks if there is a pon that already belongs to the player in the column line
  bool Board::fuyoPresent(size_t const column) {
    for(size_t row = 0; row < rows_; ++row) {
      auto Spot = at(row, column);
      if ((Spot.first) && (Spot.first -> type() == Piece::Type::fuyo) && Spot.second==turnCounter_)
        return true;
    }
    return false;
  }

  // ------------------------------------- Function telling the validity of a wished move -------------------------------
  // Function outputing the possible move theoretically
  // (only considering the piece position on board, without any other piece-blocking-way constraint)
  std::vector<std::array<int, 2>> Board::TheoreticalPhysicalMoves(Piece const * const piece,Position const InitPosition={-1,-1}){

    // Initialisations and declarations
    std::vector<std::array<int, 2>> TheoreticalMoveList; // The table containing the free corrdinates of each move (NbMoves x2, where the columns are row and columns, respectively)
    std::array<int, 2> BufferMoves;
    int maxx = Board::columns_;
    int maxy = Board::rows_;
    Position position;

    // Considering the initial position as a theoretical one when the argument is give,
    // or as the piece one when no argument is given
    if (InitPosition==Position({-1,-1})){
      position = piece->position();
    } else {
      position = InitPosition;
    }

    // Check the piece action range to chek for
    int a = std::max({abs(maxx-x(position)), abs(maxy-y(position)), abs(maxx+x(position)), abs(maxy+y(position))});

    // Check the range of moves that is possible according to the piece definition
    for(auto &move : piece->moves()) {
      if(move.second) {
        for(int idx = 1; idx <= a; idx++) {
          int Posx = move.first[0] * idx + x(position);
          int Posy = move.first[1] * idx + y(position);

          if ((Posx >= 0) && (Posx <= static_cast<int>(Board::columns_)) && (Posy>=0) && (Posy<static_cast<int>(Board::rows_))) {
            BufferMoves = {{Posy, Posx}};
            TheoreticalMoveList.push_back(BufferMoves);
          }
        }
      } else {
        int Posx = move.first[0] + x(position);
        int Posy = move.first[1] + y(position);

        if ((Posx>=0) && (Posx < static_cast<int>(Board::columns_)) && (Posy>=0) && (Posy< static_cast<int>(Board::rows_))) {
          BufferMoves = {{Posx,Posy}};
          TheoreticalMoveList.push_back(BufferMoves);
        }
      }
    }

    // Returning the set of theoretical move list
    return(TheoreticalMoveList);
  }

  // Function outputing the possible moves for the piece given the physical constraints (board limits and self/opponent pieces obtacles)
  std::vector<std::array<int, 2>> Board::PhysicalMoves(Piece const * const piece, Position const  InitPosition={-1,-1}){

    // Initialisations and declarations
    std::vector<std::array<int, 2>> PhysicalMovesList; // The table containing the free corrdinates of each move (NbMoves x2, where the columns are row and columns, respectively)
    std::array<int, 2> BufferMoves;
    int span = 1;
    Position position;

    // Considering the initial position as a theoretical one when the argument is give,
    // or as the piece one when no argument is given
    if (InitPosition==Position({-1,-1})){
      position = piece->position();
    } else {
      position = InitPosition;
    }

    // Check that the piece is an actual piece, and if not return the freshly allocated (non initialised) list
    if (!piece) return PhysicalMovesList;

    // Check the piece action range to chek for
    int maxx = static_cast<int>(Board::columns_);
    int maxy = static_cast<int>(Board::rows_);
    int maxspan = std::max({(maxx-x(position)), (maxy-y(position)), (maxx+x(position)), (maxy+y(position))});

    // Check if the move is valid according to the physical definition and deployed pieces layout
    for(auto &move : piece->moves()) {
      // Select the span depending on whether the type of move is a long-distance one or a precise location spot
      if  (move.second) {span = maxspan;}
      else              {span = 1;}

      // Loop over the long-distance span of the piece
      for(int idx = 1; idx <= span; idx++) {
        // Computing the currently tested position
        int Posx = move.first[0]*idx + x(position);
        int Posy = move.first[1]*idx + y(position);

        // Checking that the spot is within the board
        if((Posx >= 0) && (Posx < static_cast<int>(Board::columns_)) && (Posy >=0) && (Posy < static_cast<int>(Board::rows_))) {
          // Checking if there is a piece there
          auto Spot = at(static_cast<size_t>(Posy), static_cast<size_t>(Posx));
          if (Spot.first) {
            // Add as a possible move only if the piece belongs to the opponent and is not the king
            if (Spot.first-> type() != Piece::Type::osho && Spot.second==tocheck_) {
              BufferMoves = {{Posx, Posy}};
              PhysicalMovesList.push_back(BufferMoves);
            }
            // Stop the run
            break;
          } else {
            // Add the spot to a physical move
            BufferMoves = {{Posx,Posy}};
            PhysicalMovesList.push_back(BufferMoves);
          }
        }
      }
    }
    // Returning the set of physical move list
    return(PhysicalMovesList);
  }

  // Function outputing the possible moves for the piece given the physical and check constraints
  std::vector<std::array<int, 2>> Board::ContextualMoves(Piece const * const piece){
    // Function outputing the possible moves for the piece given the physical and check constraints
    // Useless to check a move validity, but may be useful to user display of possible moves

    // ----------------- Initialisation ------------------------------------------------------------------
    std::vector<std::array<int, 2>> ContextualMovesList;

    // ----------------- Checking all possible moving spots --------------------------------------------
    // Getting the physical moves
    auto Moves = PhysicalMoves(piece);

    // Filtering out the contextual moves depending on the check impact
    for (auto &Target : Moves) {
      // Check if it is a valid move
      bool OkToMove = ValidMove(piece, {x(Target),y(Target)});
      if (OkToMove) {ContextualMovesList.push_back({{x(Target),y(Target)}});}
    }

    // Returning the set of physical move list
    return(ContextualMovesList);
  }

  // Function getting the contextual moves and filtering them depending on the rules and check impact
  std::vector<std::array<int, 2>> Board::ContextualDrops(Piece const * const piece){
    // Function getting the contextual moves and filtering them depending on the check impact
    // Overkill spanning the board

    // ----------------- Initialisation ------------------------------------------------------------------
    std::vector<std::array<int, 2>> ContextualDropsList;

    // ----------------- Checking all possible dropping spots --------------------------------------------
    for (int idx = 0; idx < static_cast<int>(Board::columns_); idx++) {
      for (int idy = 0; idy < static_cast<int>(Board::rows_); idy++) {
        // Check if it is a valid drop
        bool OkToDrop = ValidDrop(piece,{idx, idy});
        if (OkToDrop) {ContextualDropsList.push_back({{idx, idy}});}
      }
    }

    // Returning the set of physical move list
    return(ContextualDropsList);
  }

  // Function closing the turn flipping the board and flipping the pieces indexing
  bool Board::CloseTurn() {
    // Checking if the opponent is checked or checkmate
    if (CheckMate(turnCounter_)) {
      emit(gameWin());
      return true;
    } else if (Check(turnCounter_)) {
      emit(check());
    }

    // Get the hand to the opponent
    advanceTurn();
    // Reversing the board indexing and cell disposition
    std::reverse(board_.begin(), board_.end());

    // Updating the location of the pieces according to the new board layout
    for(int idx = 0; idx < static_cast<int>(Board::columns_); idx++) {
      for(int idy = 0; idy < static_cast<int>(Board::rows_); idy++) {
        // Retrieving the cell of the board
        auto Spot = at(static_cast<size_t>(idy), static_cast<size_t>(idx));

        // Checking if there is a piece there and if yes, gather it
        if (Spot.first) {
          auto pos = Spot.first->position();
          Position pos2 = {static_cast<int>(columns_)-1-x(pos),static_cast<int>(rows_)-1-y(pos)};
          Spot.first->displace(pos2);
        }
      }
    }
    // Returning success of the function
    return(true);
  }

  // Checks if the considered player is checking the other one
  bool Board::Check(PlayerId consideredPlayer) {
    // Initialisations
    PlayerId Player = PlayerId::none;
    PlayerId Opponent = PlayerId::none;

    // Mapping towards the considerd protecting/attacking side wished by the user
    if (consideredPlayer == tocheck_) {
      Player    = tocheck_;
      Opponent  = turnCounter_;
    } else if (consideredPlayer == turnCounter_) {
        Player    = turnCounter_;
        Opponent  = tocheck_;
    } else {
      //!< \todo raise error in case of none player
    }

    // Determining if the player is checking the other one
    auto PlayerProtectedSpots = ProtectedSpots(Player);
    Position King = GetKingPosition(Opponent);
    bool Checked  = TwoDIntVectorMembership(PlayerProtectedSpots,King);

    return Checked;
  }

  // Checks if the considered player is checking mate the other one
  bool Board::CheckMate(PlayerId consideredPlayer) {

    // Initialisations
    PlayerId Player = PlayerId::none;
    PlayerId Opponent = PlayerId::none;

    // Mapping towards the considerd protecting/attacking side wished by the user
    if (consideredPlayer == tocheck_) {
      Player    = tocheck_;
      Opponent  = turnCounter_;
    } else if (consideredPlayer == turnCounter_) {
        Player    = turnCounter_;
        Opponent  = tocheck_;
    } else {
      //!< \todo raise error in case of none player
    }

    // Checking the check mate case
    if (Check(Player)) {
      auto Attack = AttackRange(Opponent);
      if (Attack.size()==0)
        return true;
    }
    return false;
  }

  // ------------------------------------- Actual action checking and action-doing functions -----------------------------
  // Function checking the move
  bool Board::ValidMove(Piece const * const piece, Position const position){
    // Check that we are actually dealing with a non-none piece
    if (!piece)
      {std::cout<<"There is no piece there"<<std::endl;
      return false;}

    // Get king position if the moved piece is the King
    Position King;
    if   (piece->type() == Piece::Type::osho) {King = position;}
    else                                      {King = GetKingPosition(turnCounter_);}

    // Check if the piece is on the board
    if (!piece->deployed())
      {std::cout<<"The piece is not on the board."<<std::endl;
      return false;}

    // Check that the piece belongs to the current player
    Position Pos = piece->position();
    if (!(at(static_cast<size_t>(y(Pos)),static_cast<size_t>(x(Pos))).second == turnCounter_))
      {std::cout<<"The piece is not yours"<<std::endl;
      return false;}

    // Check if the wished move belongs to the PhysicalMoves of the piece
    auto Moves = PhysicalMoves(piece); bool flag = false;
    for (auto &move: Moves) {if (std::equal(std::begin(move), std::end(move), std::begin(position))) {flag = true; break;}}
    if  (flag==false)
      {std::cout<<"The piece cannot physically move there"<<std::endl;
      return false;}

    // Check if the move does not make the king checked
    Piece *NewPiece  = new Piece(piece->type(),static_cast<unsigned int>(x(position)),static_cast<unsigned int>(y(position)));
    auto OtherPlayerProtectedSpots = ProtectedSpots(tocheck_,*NewPiece,*piece);
    bool Checked = TwoDIntVectorMembership(OtherPlayerProtectedSpots,King);
    if (Checked)
      {std::cout<<"This move would make you checked, or you are still checked after this move"<<std::endl;
      return false;}

    // If we passed all, we are good: update the board (or do another routine for that)
    return true;
  }

  // Function checking the drops
  bool Board::ValidDrop(Piece const * const piece, Position const position){
    // Check that we are actually dealing with a non-none piece
    if (!piece)
      return false;

    // Check if the piece is in one's hand
    if (canDrop(turnCounter_,  piece->type()))
      return false;

    // Check if the selected piece is actually valid for a drop
    if (!piece->droppable())
      return false;

    // Check if the position is within the board
    if ((x(position)<0) || (x(position) >= static_cast<int>(Board::rows_)) || (y(position) >= static_cast<int>(Board::columns_)) || (y(position)<0))
      return false;

    // Check if the spot is free
    if (at(static_cast<size_t>(y(position)),static_cast<size_t>(x(position))).first)
      return false;

    // Checking if the dropped piece would have theoretically physical moves
    auto Moves = TheoreticalPhysicalMoves(piece, position);
    if (Moves.size() == 0)
      return false;

    // Check if the drop is valid: check if there is no checkmate if a pon drop
    auto Attack = AttackRange(tocheck_,*piece);
    if (piece->type() == Piece::Type::fuyo && Board::PieceIsChecking(piece, position) && Attack.size()==0)
      return false;

    // Check if the drop is valid: check if there is no other pon on the column belonging to the same player
    if (piece->type() == Piece::Type::fuyo && Board::fuyoPresent(static_cast<size_t>(x(position))))
      return false;

    // In the case where the user is checked, the drop should remediate to that
    auto Spots = ProtectedSpots(tocheck_,*piece);
    Position King    = GetKingPosition(turnCounter_);
    bool flag        = TwoDIntVectorMembership(Spots,King);
    if (flag==true)
      return false;

    // If we passed all, we are good: update the board (or do another routine for that)
    return true;
  }

  // Function doing the move
  bool Board::move(Piece * piece, Position const position){
    // Checking if the wished move is valid
    if (!ValidMove(piece, position)) {return false;}

    // If we are here, we are good: update the board (or do another routine for that)
    // Remove the information contained in the cell at the previous location
    auto PreviousPosition = piece->position();
    at({y(PreviousPosition),x(PreviousPosition)}) = {nullptr, PlayerId::none};

    // Do the eaten piece principle
    //!< \todo actually eat and demote
    auto PreviousPiece = at({y(PreviousPosition),x(PreviousPosition)}).first;
    if (PreviousPiece) {
      emit(pieceEaten(*PreviousPiece));
    }

    // Add the piece to the target cell
    piece->move(position);
    at({y(position),x(position)}) = {piece, turnCounter_};

    // If the piece entered promotion area or left the promotion area while not being promoted,
    // emit the relevant signal
    bool CanBePromoted = ((isPromotionArea(position, turnCounter_) || (!isPromotionArea(position, turnCounter_) && isPromotionArea(PreviousPosition, turnCounter_))));
    bool IsNotPromotedAlready = !piece->promoted();
    if (CanBePromoted && IsNotPromotedAlready) {
      emit(enteredPromotionArea(*piece));
    }

    // Close up
    CloseTurn();
    return true;
  }

  // Function doing the drop
  bool Board::drop(Piece const * const piece, Position const position){

    // Checking if the wished drop is valid
    if (!ValidDrop(piece, position)) {return false;}

    // Deploy the piece
    doDrop(turnCounter_, piece->type());
    Piece *tmp = new Piece(piece->type(),static_cast<unsigned int>(x(position)),static_cast<unsigned int>(y(position)));
    at(static_cast<size_t>(y(position)),static_cast<size_t>(x(position))) = {tmp, turnCounter_};

    // If we are here, we are good: close up
    CloseTurn();

    // Return true
    return true;
  }
}
