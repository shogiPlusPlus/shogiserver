/***************************************************************************
 * Copyright (C) 2021 Francesco Florian, Élise Le Mélédo
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef PIECES_H
#define PIECES_H

#include <array>
#include <list>
#include <string>

#include <QtCore/QDataStream>

#include "types.h"

namespace ShogiServer {
  /**
   * A set of possible moves
   *
   * * The first item is the relative move;
   * * The second item specifies wether positive multiples are allowed.
   */
  using Move = std::pair<std::array<int, 2>, bool>;
  using MovesList = std::list<Move>; //!< List of moves

  /**
   * Stores information about one piece.
   *
   * The piece is intended to be moved and managed by a combination of board and player, this class checks whether the requested actions (drop, move) are legal for this piece type,
   * both directly and via helper functions giving all possible moves.
   */
  class Piece
  {
  public:
    /**
     * List of all possible pieces.
     *
     * The function baseTypesCount assumes some ordering of the names in this list.
     * \todo document them with original name (and moves?)
     */
    enum class Type : unsigned char {
      fuyo,
      gin,
      hisha,
      kakugyo,
      keima,
      kin,
      kyosha,
      osho,
      promotedFuyo,
      promotedHisha,
      promotedKakugyo,
      promotedKeima,
      promotedKin,
      promotedKyosha,
      none
    };
    inline static constexpr size_t typesCount() {
      return static_cast<size_t>(Type::none);
    }                                                   //!< Get the total count of valid pieces. \return Pieces count
    inline static constexpr size_t baseTypesCount() {
      return static_cast<size_t>(Type::osho) + 1;
    }                                                   //!< Get the total count of non promoted pieces. Assumes some ordering of the pieces list. \return Pieces count

    //! Constructor. Create a new piece with no position.
    Piece(Type const type       //!< Type of the piece.
          );
    //! Constructor. Create a new piece reading its serialied representiation from a stream.
    Piece(QDataStream &in       //! Input stream to read the data.
          );
    Piece(Piece const &) = default;
    Piece(Piece &&) = default;
    Piece(Type const type, unsigned int posx, unsigned int posy);
    Piece& operator=(Piece const &) = default;
    Piece& operator=(Piece &&) = default;

    /**
     * Return this piece to its non promoted state.
     */
    void demote();
    /**
     * Determine if the piece is on the board.
     *
     * The actual check is whether the coordinates are non negative: since the piece has no knowdlege of the size of the board, it cannot check that it is indeed inside.
     *
     * @return Whether this piece is placed on the board.
     */
    bool deployed() const;
    /**
     * Drop the piece at the specified coordinates.
     *
     * Checks with is_droppable whether this operation is legal first.
     *
     * @param x x coordinate for the drop.
     * @param y y coordinate for the drop.
     * @return True if the piece could be dropped, false otherwise
     */
    bool drop(int const x, int const y);
    /**
     * Determine if the piece is droppable.
     *
     * @return Whether this piece can be dropped.
     */
    bool droppable() const;
    /**
     * Move the piece.
     *
     * Check that the move is actually a legal move before performing it.
     * @todo Someone should check that I got the unbounded part right.
     *
     * @return Whether the operation was successful.
     */
    bool move(Position const position //!< New position.
              );

    /**
     * Get all te possible moves of the piece.
     *
     * @todo A function that checks whether a move is legal would be much more useful if the piece has "unbounded" moves, but it won't work here.
     *
     * @return the list of possible moves.
     */
    MovesList moves() const;
    /**
     * Promote this piece (if possible).
     */
    void promote();
    /**
     * Check whether this piece is a promodet one.
     *
     * @return Whether this piece is promoted.
     */
    bool promoted() const;
    /**
     * Remove from the board (and possibly demotes) the piece.
     *
     * Set the position to the invalid value {-1, -1}; if the piece was promoted, return it to the base state.
     */
    void remove();
    /**
     * Human-(almost)readable description of the piece internals
     */
    std::string string() const {
      return std::to_string(position_[0]) + ',' + std::to_string(position_[1]) + "; Type: " + std::to_string(static_cast<int>(type_)) + '\n';
    }
    /**
     * Arbitrarily displaces the piece to the target position (useful for board flip)
     */
    bool displace(Position const position);

    /**
     * Get type.
     *
     * @return Piece type
     */
    Type type() const { return type_; }
    /**
     * Get the position.
     *
     * @return Piece position
     */
    Position position() const { return position_; }
  private:
    Position position_ = {-1,-1};
    Type type_ = Type::none;
  };

  inline QDataStream & operator<<(QDataStream &out, Piece const piece) {
    return out << piece.type() << piece.position();
  }
  inline QDataStream & operator>>(QDataStream &in, Piece &piece) {
    piece = Piece(in);
    return in;
  }

  using PieceNames = std::array<std::string, Piece::typesCount()>;
  //! Names of the pieces as strings; the order MUST be the same as in the Piece::Type enum
  static const PieceNames pieceNames {
    "fuhyo",
    "gin",
    "hisha",
    "kakugyo",
    "keima",
    "kin",
    "kyosha",
    "osho",
    "promotedFuhyo",
    "promotedHisha",
    "promotedKakugyo",
    "promotedKeima",
    "promotedKin",
    "promotedKyosha"
  };
}

#endif /* PIECES_H */
