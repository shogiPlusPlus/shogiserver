/***************************************************************************
 * Copyright (C) 2021 Francesco Florian, Élise Le Mélédo
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef SERVER_H
#define SERVER_H

#include <list>
#include <memory>

#include <QtCore/QObject>

#include "board.h"
#include "player.h"

namespace ShogiServer {
  /**
   * Connect to every Player, init the Board and manage the game.
   */
  class Server
    : public QObject
  {
    Q_OBJECT
  public:
    /**
     * Add a Player
     */
    bool addPlayer(Player* const player,        //!< New player.
                   Board::PlayerId const role   //!< Determine who starts; if Board::PlayerId::none, than the player will never move.
                   );
    /**
     * Switch the first and second player.
     *
     * \todo Check that the game has not started yet.
     */
    void switchPlayerRoles();
    /**
     * Promote a Player who is not playing to a playing position.
     * The Player must be in the list of current players and not have a playing role already.
     * Moreover, the requested role should be available.
     *
     * \return Whether operation was successful.
     */
    bool promotePlayer(Player * const player,           //!< Player to promote.
                       Board::PlayerId const role       //!< Player role; if Board::PlayerId::none, accept any available role.
                       );
    /**
     * Force the game start even if not all players are ready.
     * Nevertheless, at least two players must be available.
     *
     * \note In case the games actually starts, this function will only return after the game is finished;
     * for this reason the function does not return, it emits gameStarted carrying information on wether the game has started instead.
     */
    void forceGameStart();
    /**
     * Start the game if all players are ready.
     *
     * \note In case the games actually starts, this function will only return after the game is finished;
     * for this reason the function does not return, it emits gameStarted carrying information on wether the game has started instead.
     */
    void startIfReady();
  signals:
    void gameStarted(bool success);                    //!< Signal eitted when trying to start the game. @param success is true if the game has actually started.
    void gameEnded(Board::PlayerId const winner);      //!< Signal eitted at the game end. @param winner The winner player.
  protected:
    /**
     * Main function that plays the game.
     */
    void play();
    /**
     * Check whether the given player is present in the board list.
     *
     * \return True if it is found.
     */
    bool hasPlayer(Player const * const player          //!< Pointer to the player to check;
                   );
    /**
     * Create or recreate the signal-slot connections associated to a given player.
     * All previous connections are cleared first.
     */
    void createReadyConnections(Board::PlayerId const role     //!< Which player to (re)connect
                           );
    Board board_;                                       //!< Game board.
    std::list<std::unique_ptr<Player>> players_;        //!< List of Player who are either observing or playing.
    Player *first_ = nullptr;                           //!< First Player.
    Player *second_ = nullptr;                          //!< Second Player.
    bool firstReady_ = false;                           //!< Whether the first player is ready
    bool secondReady_ = false;                          //!< Whether the second player is ready
  };
}

#endif /* SERVER_H */
