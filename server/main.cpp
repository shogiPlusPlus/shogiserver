/***************************************************************************
 * Copyright (C) 2021 Francesco Florian, Élise Le Mélédo
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#include "piece.h"
#include "board.h"

#include <iostream>

using namespace ShogiServer;
using namespace std;

int main(int argc [[maybe_unused]], char** argv [[maybe_unused]]) {

  auto b = Board();
  int x, y,x1,y1;

  for (int u=0; u<=100; ++u){
    cout << "==============================="<<endl;
    if (b.GetTurn()==Board::PlayerId::first) {
      cout << "Player first hand"<<endl;
    } else {
      cout << "Player second hand"<<endl;
    }
    cout << "------------------"<<endl;
    cout<<endl<<  "            Game          "<< "                           "<< "Positions"<< "                            "<< "Belongings"<<endl;
    cout<< "  | "; for (int j=0; (unsigned int) j<Board::columns_;j++) {cout<< j << "  ";}
    cout<<endl << "------------------------------"<<endl;
    for (int i=Board::rows_-1; i>=0;i--) {
      cout<< i << " | ";
      for (int j=0; (unsigned int) j<Board::columns_;j++) {
        auto Spot = b.at({i,j});
        if (Spot.first)
        {cout<< "1  ";}
        else {cout<< "0  ";}
      }

      cout << "            ";
      for (int j=0; (unsigned int) j<Board::columns_;j++) {auto Spot = b.at({i,j});
        if (Spot.second==Board::PlayerId::first)
        {Position xy = Spot.first->position() ;cout<<xy[0]<<xy[1]<<" ";}
        else if (Spot.second==Board::PlayerId::second){Position xy = Spot.first->position() ;cout<<xy[0]<<xy[1]<<" ";}
        else{cout<< "0  ";}
      }

      cout << "            ";
      for (int j=0; (unsigned int) j<Board::columns_;j++) {auto Spot = b.at({i,j});
        if (Spot.second==Board::PlayerId::first)
        {cout<< "1 "<<" ";}
        else if (Spot.second==Board::PlayerId::second){cout<< "2 "<<" ";}
        else{cout<< "0  ";}
      }
      cout<<endl;
    }

  cout << "Enter x and y init position"<<endl;
  cin >> x >> y;
  cout << "Enter target position"<<endl;
  cin >> x1>>y1;

  Position original = {x,y}; Position position = {x1,y1};     // Position in xy
  auto piece = b.at(original[1], original[0]).first;          // index for at in in row-column
  auto valid = b.move(piece,position);
  cout << "From "<< original[0]<<original[1]<< " to "<< position[0]<<position[1]<<   ", valid move :"<<valid <<endl <<endl;
  }
}
