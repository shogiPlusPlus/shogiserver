/***************************************************************************
 * Copyright (C) 2021 Francesco Florian, Élise Le Mélédo
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#include "player.h"

#include <QtCore/QDataStream>
#include <QtCore/QIODevice>
#include <QtCore/QObject>

#include "piece.h"
#include "logger.h"

namespace ShogiServer {
  QDataStream &	operator<<(QDataStream &out, Player::Move const move) {
    return out << move.type << move.piece << move.finalPosition;
  }
  QDataStream &	operator>>(QDataStream &in, Player::Move &move) {
    return in >> move.type >> move.piece >> move.finalPosition;
  }

  PlayerNetworkReceiver::ReactionFunction PlayerNetworkReceiver::doNothing = [](MessageType const type [[maybe_unused]]) {};
  PlayerNetworkReceiver::ReactionFunction PlayerNetworkReceiver::invalidMessage = [](MessageType const type) {
    Logger::warn("Received invalid message; index: " + std::to_string(static_cast<int>(type)));
  };
  
  PlayerNetworkReceiver::PlayerNetworkReceiver(Reactions customActions)
    : reactions_(customActions) {}
  PlayerNetworkReceiver::~PlayerNetworkReceiver() {
    if(channel_ == nullptr)
      return;
    channel_ -> close();
    delete channel_;
  }
  bool PlayerNetworkReceiver::connection(QIODevice * const channel) {
    if((channel->openMode() & QIODevice::ReadWrite) == QIODevice::ReadWrite) {
      channel_ = channel;
      stream_.setDevice(channel);
      emit ready();
      return true;
    }
    return false;
  }
  void PlayerNetworkReceiver::readMessage() {
    stream_.startTransaction();
    MessageType type;
    stream_ >>  type;
    if(! stream_.commitTransaction())    //check again when new data become available.
      return;

    if(stream_.status() == QDataStream::ReadCorruptData)
      return sendCorrupted();

    if(type > MessageType::none)
      return sendCorrupted();

    react(type);
  }

  void PlayerNetworkReceiver::play(Move const &move) {
    stream_ << move;
    reaction(MessageType::move) = [&](MessageType type [[maybe_unused]]) {
      tryReadMove();
    };
    reaction(MessageType::corrupted) = [=, this](MessageType type [[maybe_unused]]) {stream_ << move;};
    reaction(MessageType::answerOk) = [&](MessageType type [[maybe_unused]]) {
      reaction(MessageType::corrupted) = invalidMessage;
      reaction(MessageType::answerOk) = invalidMessage;
    };
  }
  void PlayerNetworkReceiver::addPiece(Piece const  &piece) {
    stream_ << MessageType::gotPiece << piece;
    reaction(MessageType::corrupted) = [=, this](MessageType type [[maybe_unused]]) {stream_ << MessageType::gotPiece << piece;};
    reaction(MessageType::answerOk) = [&](MessageType type [[maybe_unused]]) {
      reaction(MessageType::corrupted) = invalidMessage;
      reaction(MessageType::answerOk) = invalidMessage;
    };
  }
  void PlayerNetworkReceiver::removePiece(Piece const &piece) {
    stream_ << MessageType::lostPiece << piece;
    reaction(MessageType::corrupted) = [=, this](MessageType type [[maybe_unused]]) {stream_ << MessageType::lostPiece << piece;};
    reaction(MessageType::answerOk) = [&](MessageType type [[maybe_unused]]) {
      reaction(MessageType::corrupted) = invalidMessage;
      reaction(MessageType::answerOk) = invalidMessage;
    };
  }


  void PlayerNetworkReceiver::tryReadMove() {
    stream_.startTransaction();
    Move move{.type = MoveType::invalid, .piece{Piece::Type::none}, .finalPosition{invalidPosition()}};
    stream_ >> move;
    if(! stream_.commitTransaction())    //check again when new data become available.
      return;

    if(stream_.status() == QDataStream::Status::ReadCorruptData) {
      sendCorrupted();
    } else {
      sendOk();
      emit(turnDone(move));
#warning promotion?
      stream_.setStatus(QDataStream::Status::Ok);
      reaction(MessageType::move) = invalidMessage;
      reaction(MessageType::corrupted) = invalidMessage;
    }
  }
  void PlayerNetworkReceiver::sendOk(){
    stream_ << MessageType::answerOk;
  }

  void PlayerNetworkReceiver::sendCorrupted() {
    (void) channel_->readAll();
    stream_ << MessageType::corrupted;
  }
}
