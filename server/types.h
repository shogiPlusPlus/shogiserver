/***************************************************************************
 * Copyright (C) 2021 Francesco Florian, Élise Le Mélédo
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef TYPES_H
#define TYPES_H

#include <array>
#include <QtCore/QDataStream>

namespace ShogiServer {
  /**
   * Position type.
   *
   * A vector of two integers. If any of them is (strictly) negative, it is assumed to be an invalid position.
   */
  using Position = std::array<int, 2>;
  inline constexpr int& x(Position &position) { return position[0]; }   //!< Get a reference to the x coordinate.
  inline constexpr int& y(Position &position) { return position[1]; }   //!< Get a reference to the y coordinate.
  inline constexpr int const& x(Position const &position) { return position[0]; }   //!< Get a reference to the x coordinate.
  inline constexpr int const& y(Position const &position) { return position[1]; }   //!< Get a reference to the y coordinate.
  inline constexpr Position invalidPosition() { return {-1,-1}; }       //!< Get an invalid Position.
  inline constexpr bool valid(Position const position) {return (x(position) >= 0 && y(position) >= 0); }        //!< Determine if the posiion is valid.
  QDataStream & operator<<(QDataStream &out, Position const position);
  QDataStream & operator>>(QDataStream &in, Position &position);

  inline std::string libraryInfo() {
    return std::string("Library") + cmake_PROJECT_NAME + " version " + std::to_string(cmake_MAJOR) + '.' +  std::to_string(cmake_MINOR) + ".\n" +
      cmake_PROJECT_NAME + "is free software, and you are welcome to redistribute it under certain conditions.\n" + 
      "For details check either the files COPYING and COPYING.LESSER or https://www.gnu.org/licenses/gpl-3.0-standalone.html and https://www.gnu.org/licenses/lgpl-3.0-standalone.html.";
  }
}

#endif /* TYPES_H */
