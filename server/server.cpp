/***************************************************************************
 * Copyright (C) 2021 Francesco Florian, Élise Le Mélédo
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "server.h"

#include <list>
#include <memory>

#include <QtCore/QObject>
#include <QtCore/QThread>

#include "logger.h"

#include "board.h"
#include "player.h"

namespace ShogiServer {
  bool Server::addPlayer(Player* const player, Board::PlayerId const role) {
    //! \todo check that it is unique? (otherwise unique_ptr may cause problems.)
    //! (alternative: change to shared. is it useful?)
    //! \todo should this directly take a shared_ptr/unique_ptr?
    players_.emplace_front(player);
    if(role == Board::PlayerId::none)
      return true;
    return promotePlayer(player, role);
  }
  void Server::switchPlayerRoles() {
    //! \todo ask the players first.
    std::swap(first_, second_);
    firstReady_ =  secondReady_ = false;
    createReadyConnections(Board::PlayerId::first);
    createReadyConnections(Board::PlayerId::second);
  }
  bool Server::promotePlayer(Player * const player, Board::PlayerId const role) {
    switch(role) {
    case Board::PlayerId::none:
      return promotePlayer(player, Board::PlayerId::first) || promotePlayer(player, Board::PlayerId::second);
    case Board::PlayerId::first:
      if((first_ != nullptr) || (second_ == player) || !hasPlayer(player))
        return false;
      first_ = player;
      createReadyConnections(Board::PlayerId::first);
      return true;
    case Board::PlayerId::second:
      if((second_ != nullptr) || (first_ == player) || !hasPlayer(player))
        return false;
      second_ = player;
      createReadyConnections(Board::PlayerId::second);
      return true;
    }
    return false; //function should never reach this point, but needed to silence the warnings
  }
  void Server::forceGameStart() {
    if(players_.size() < 2) {
      emit(gameStarted(false));
      return;
    }

    //! \todo This ovekill should safely works with few players, but it will take long with a lot of them. Check whether the "few players" assumption is true.
    for(auto &item : players_)
      (void) promotePlayer(item.get(), Board::PlayerId::none);

    if(first_ == nullptr || second_ == nullptr) {
      Logger::warn("Server::forceGameStart: Could not fill all game roles, although sufficient players are available. Aborting game start.", Logger::WarnLevel::error);
      emit(gameStarted(false));
      return;
    }
    emit(gameStarted(true));
    play();
  }
  void Server::startIfReady() {
    if(firstReady_ && secondReady_) {
      forceGameStart();
    } else {
      emit(gameStarted(false));
      return;
    }
  }

  void Server::play() {
    if(first_ == nullptr || second_ == nullptr) {
      Logger::warn("Server::play: Game has started, but not enough players were available. Stopping with no winner.", Logger::WarnLevel::error);
      emit(gameEnded(Board::PlayerId::none));
      return;
    }
    // Disconnect all slots
    first_->disconnect();
    second_->disconnect();

    // Set variables describing the current turn
    Player *currentPlayer = first_;
    Player *otherPlayer = second_;
    Board::PlayerId currentId = Board::PlayerId::first;
    Board::PlayerId otherId = Board::PlayerId::second;

    // Setup a fake "last move" to use the first turn
    Player::Move lastMove {.type = Player::MoveType::invalid, .piece{Piece::Type::none}, .finalPosition{-1,-1}};

    connect(&board_, &Board::pieceEaten, [&](Piece const &piece){        // Capturing by reference: swapping not needed!
      otherPlayer->removePiece(piece);
      currentPlayer->addPiece(piece);
    });

    for(;;) {
      connect(currentPlayer, &Player::turnDone, this, [&](Player::Move move){ lastMove = move; });
      //! \todo right now the player has to figure out everything based on last move... give more help.
      currentPlayer->play(lastMove);
      switch(lastMove.type) {
      case Player::MoveType::invalid:
        Logger::warn("Server::play: Player returned invalid move type; treating as surrender.", Logger::WarnLevel::note);
        [[fallthrough]];
      case Player::MoveType::surrender:
        emit(gameEnded(otherId));
        return;
      case Player::MoveType::drop:
        if(board_.drop(&lastMove.piece, lastMove.finalPosition))
          break;
        Logger::warn("Server::play: Player returned invalid drop; treating as surrender.", Logger::WarnLevel::note);
        emit(gameEnded(otherId));
        return;
      case Player::MoveType::move:
        if(board_.move(&lastMove.piece, lastMove.finalPosition))
          break;
        Logger::warn("Server::play: Player returned invalid move; treating as surrender.", Logger::WarnLevel::note);
        emit(gameEnded(otherId));
        return;
      }
      currentPlayer->disconnect();
      //! \todo probably also turn the board.
      std::swap(currentPlayer, otherPlayer);
      std::swap(currentId, otherId);
    }
  }

  bool Server::hasPlayer(Player const * const player) {
    if(player == nullptr)       //probably useless, but it cannot harm
      return false;
    for(auto &item : players_)
      if(item.get() == player)
        return true;

    return false;
  }
  void Server::createReadyConnections(Board::PlayerId const role) {
    switch(role) {
    case Board::PlayerId::none:
      break;
    case Board::PlayerId::first:
      if(first_ != nullptr) {
        first_->disconnect();
        connect(first_, &Player::ready, this, [&](){ this->firstReady_ = true; });
        connect(first_, &Player::notReady, this, [&](){ this->firstReady_ = false; });
      }
      break;
    case Board::PlayerId::second:
      if(second_ != nullptr) {
        second_->disconnect();
        connect(second_, &Player::ready, this, [&](){ this->secondReady_ = true; });
        connect(second_, &Player::notReady, this, [&](){ this->secondReady_ = false; });
      }
      break;      
    }
  }
}
