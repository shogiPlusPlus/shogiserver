/***************************************************************************
 * Copyright (C) 2021 Francesco Florian, Élise Le Mélédo
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef PLAYER_H
#define PLAYER_H

#include <QtCore/QDataStream>
#include <QtCore/QIODevice>
#include <QtCore/QObject>
#include <functional>

#include "piece.h"

namespace ShogiServer {
  /**
   * Virtual class for a generic player.
   *
   * The board will usually assume that the player may cheat, therefore it will not rely on the information provided by the player other that its moves (i.e., the return value of the 'play' function).
   * However, a copy of the board and all useful events are given to the player, so that it can plan its strategy.
   */
  class Player
    : public QObject {
    Q_OBJECT
  public:
    /**
     * Type of the Player action.
     */
    enum class MoveType {
      surrender,//!< Surrender to lose and quit the match.
      move,     //!< Move a Piece. This requires the begin and end coordinate.
      drop,     //!< Drop a Piece. This requires the drop coordinate and the piece Type.
      invalid   //!< Invalid move. The board can decide how to manage it (e.g., call the "play" function again). It is also used to communicate the last opponent move when there is none (i.e., at the first move).
    };
    /**
     * A move as communicated by the Player.
     *
     * Depending on the type, some elements may be ignored.
     */
    struct Move {
      MoveType type;            //!< Type of the move.
      Piece piece;              //!< Piece to move. The board will not usually try to match the piece exactly; instead, this parameter will be used to extract either the type or the coordinates, depending on the move type.
      Position finalPosition;   //!< Final position of the piece. Must be on the board.
    };
    /**
     * Tell the player to move.
     * This function does not return: the turnDone signal must be used to communicate the computed move when the player has decided.
     */
    virtual void play(Move const &move //!< The last move of the opponent. Will be of invalid type at the first turn.
                      ) = 0;
    /**
     * Tell the player it got a new piece.
     *
     * If the piece position is not None (which should only happen before the game starts), then the piece is already placed on the board, otherwise it can be dropped.
     */
    virtual void addPiece(Piece const & piece   //!< The new piece.
                          ) = 0;
    /**
     * Tell the player it lost a piece.
     *
     * A reference to the Piece is given, although its Position should be sufficient.
     */
    virtual void removePiece(Piece const &piece //!< The lost piece
                             ) = 0;
  signals:
    void ready();               //!< The player is ready to start the game.
    void notReady();            //!< The player is not ready to start the game.
    void turnDone(Move);        //!< The player has computed its move.
    void promoteLastPiece(bool);//!< The player has decided whether to promove its last piece.
  };

  QDataStream &	operator<<(QDataStream &out, Player::Move const move);
  QDataStream &	operator>>(QDataStream &in, Player::Move &move);

  /**
   * A Player that receives instructions over the network.
   * It can actually also use a local socket, or anything supported by QIODevice (in theory even a file).
   *
   * @todo inherit QIODevice instead of a pointer? Needs a subclass for every type of connection...
   * @todo add some security/error recovery to the channel.
   *
   * The player sends and receives data in the following format:
   * * A MessageType identifier describing the Type;
   * * Any arguments required by that type;
   * * A newline character \todo not really.
   *
   * If the newline character is not the first data after the required arguments, all current data available for read are discarded and a MessageType is expcted as the next received type; 
   * if the current data consist of more than one message and the last one is not complete, and the first newline is missing this can result in corrupted data.
   * If the data are corrupted, all current available data are ignored, an answer with argument AnswerType::corruptedData is sent back, waiting for them to be repeated. 
   */
  class PlayerNetworkReceiver
    : public Player {
    Q_OBJECT
  public:
    /**
     * Describe the kind of message being sent to/from the remote player.
     * Depending on the type, some arguments are expected to be provided as serialized data following the type.
     * With each possible type the expected argument are described.
     */
    enum class MessageType {
      ping,     //!< Ask for an answer of any type. No arguments.
      move,     //!< Communicate a move. Arguments: Player::Move.
      lostPiece,//!< Communicate that a piece was lost. Arguments: Type.
      gotPiece, //!< Communicate that a piece was obtained. Arguments: Type.
      promotion,//!< Offer promotion. Arguments: Piece, or none (the piece to be promoted is assumed to be the last moved).
      answerYes,//!< Answer "yes" to a previuos communication. No arguments
      answerNo, //!< Answer "no" to a previuos communication. No arguments
      answerOk, //!< Answer "ok" to a previuos communication (implying it was not a question). No arguments
      corrupted,//!< Ask resend last message, as the received data is corrupted. No arguments
      none      //!< No message. No arguments.
    };
    using ReactionFunction = std::function<void(MessageType const)>;
    using Reactions = std::array<ReactionFunction, static_cast<std::underlying_type_t<MessageType>>(MessageType::none) + 1>;
    PlayerNetworkReceiver() = default;                  //!< Default constructor. Use default reactions to messages.
    PlayerNetworkReceiver(Reactions customActions       //!< Array of (initial) custom functions to call when receiving a message. They might be changed later.
                          );                            //!< Construct using custom functions to react to messages.
    virtual ~PlayerNetworkReceiver() override;          //!< Destructor: close and destroy the stream if present.
    virtual bool connection(QIODevice * const channel);
    virtual void play(Move const &move) override;
    virtual void addPiece(Piece const  &piece) override;
    virtual void removePiece(Piece const &piece) override;

    /************************************************
     * Functions to use as default reactions *
     ************************************************/
    static ReactionFunction doNothing;          //!< Do noting
    static ReactionFunction invalidMessage;     //!< Perform an appropriate action for a message type that was not expected by this player.
    
  protected:
    ReactionFunction & reaction(MessageType const action//!< Index to access
                               ) {                      //!< Get a reference to the specific reaction
      return reactions_[static_cast<size_t>(action)];
    }
    /**
     * React to a message 
     */
    void react(MessageType const action                 //!< Last received message Type.
               ) {
      reactions_[static_cast<size_t>(action)](action);
    }
    void readMessage();         //!< Try to read a message, of any type.
    void tryReadMove();         //!< Try to read a move.
    void sendOk();              //!< Send an answer AnswerType::ok.
    void sendCorrupted();       //!< Manage corrupted data, and send an answer asking for them again.

    QIODevice *channel_ = nullptr;
    QDataStream stream_;

    Reactions reactions_ = {[&](MessageType const type [[maybe_unused]]) {
      this->stream_ << MessageType::answerOk;
    },
      PlayerNetworkReceiver::invalidMessage,
      PlayerNetworkReceiver::invalidMessage,
      PlayerNetworkReceiver::invalidMessage,
      PlayerNetworkReceiver::invalidMessage,
      PlayerNetworkReceiver::invalidMessage,
      PlayerNetworkReceiver::invalidMessage,
      PlayerNetworkReceiver::invalidMessage,
      PlayerNetworkReceiver::invalidMessage,
      PlayerNetworkReceiver::invalidMessage};
  };
}

#endif /* PLAYER_H */
