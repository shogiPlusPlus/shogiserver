/***************************************************************************
 * Copyright (C) 2021 Francesco Florian, Élise Le Mélédo
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#include "piece.h"

#include <algorithm>
#include <array>
#include <list>
#include <tuple>
#include <QtCore/QDataStream>

namespace ShogiServer {
  /**
   * Data with all the information about a piece (given by the game rules).
   * A piece data has:
   * * The list of legal moves for that piece; this is a list of Piece::Move
   * * The Type of the corresponding promoted piece, or Piece::Type::none if the piece cannnot be promoted
   * * true if this is a promoted piece, false otherwise (to distinguish whether the second item is a real promotion or a demotion, which is needed when the piece is eaten).
   * @todo make the second item a lambda? or even have two lambdas, promote and reset? both look like more coding...
   */
  using TypeData = std::tuple<MovesList const, Piece::Type, bool>;

  static inline MovesList acceptMultiples(MovesList const input) {
    MovesList out = input;
    std::for_each(out.begin(), out.end(), [](Move &move){move.second = true;});
    return out;
  }
  static inline MovesList operator+(MovesList const &o1, MovesList const &o2) {
    MovesList tmp1 = o1;
    MovesList tmp2 = o2;
    tmp1.splice(tmp1.end(), tmp2);
    return tmp1;
  }

  static MovesList const movesDiagonal = {{{1, 1}, false}, {{1, -1},false}, {{-1, 1}, false}, {{-1, -1}, false}};
  static MovesList const movesHorizontalVertical = {{{1, 0}, false}, {{-1, 0}, false}, {{0, 1}, false}, {{0, -1}, false}};
  static MovesList const movesDiagonalUnbounded = acceptMultiples(movesDiagonal);
  static MovesList const movesHorizontalVerticalUnbounded = acceptMultiples(movesHorizontalVertical);
  static MovesList const movesAllForward = {{{-1, 1}, false}, {{0, 1}, false}, {{1, 1}, false}};
  static MovesList const movesAllLateral = {{{-1, 0}, false}, {{1, 0}, false}};
  static MovesList const movesDiagonalBackward = {{{-1, -1}, false}, {{1, -1}, false}};
  static MovesList const movesStraightBackward = {{{0, -1}, false}};
  static MovesList const movesGin = movesAllForward + movesAllLateral + movesStraightBackward;
  static MovesList const movesFuyo = {{{0, 1}, false}};
  static MovesList const movesKeyma = {{{1, 2}, false}, {{-1, 2}, false}};
  static MovesList const movesKin = movesAllForward + movesDiagonalBackward;
  static MovesList const movesKyosha = {{{0, 1}, true}};
  static MovesList const movesOsho = movesDiagonal + movesHorizontalVertical;
  static MovesList const movesPromodedHisha = movesHorizontalVerticalUnbounded + movesDiagonal;
  static MovesList const movesPromotedKakugyo = movesDiagonalUnbounded + movesHorizontalVertical;

  /**
   * Array of the piece data.
   * @todo Someone should check
   * @todo initialize making the names visible? Probably more readable, less safe; could try a class and constinit
   */
  static std::array<TypeData const, static_cast<std::underlying_type_t<Piece::Type>>(Piece::Type::none)> const typeData {
    std::make_tuple(movesFuyo, Piece::Type::promotedFuyo, false), //make_tuple shouldn't be needed but it won't compile without it
    {movesGin, Piece::Type::none, false},
    {movesHorizontalVerticalUnbounded, Piece::Type::promotedHisha, false},
    {movesDiagonalUnbounded, Piece::Type::promotedKakugyo, false},
    {movesKeyma, Piece::Type::promotedKeima, false},
    {movesKin, Piece::Type::promotedKin, false},
    {movesKyosha, Piece::Type::promotedKyosha, false},
    {movesOsho, Piece::Type::none, false},
    {movesGin, Piece::Type::fuyo, true},
    {movesPromodedHisha, Piece::Type::hisha, true},
    {movesPromotedKakugyo, Piece::Type::kakugyo, true},
    {movesGin, Piece::Type::keima, true},
    {movesGin, Piece::Type::kin, false},
    {movesGin, Piece::Type::kyosha, false}
  };

  static inline TypeData data(Piece::Type const type) {
    return typeData[static_cast<std::underlying_type_t<Piece::Type>>(type)];
  }

  static inline MovesList moves(TypeData const data) {
    return std::get<0>(data);
  }
  static inline Piece::Type promotion(TypeData const data) {
    return std::get<1>(data);
  }
  static inline bool promoted(TypeData const data) {
    return std::get<2>(data);
  }

  static inline MovesList moves(Piece::Type const type) {
    return moves(data(type));
  }
  static inline Piece::Type promotion(Piece::Type const type) {
    return promotion(data(type));
  }
  static inline bool promoted(Piece::Type const type) {
    return promoted(data(type));
  }
  Piece::Piece(Type const type)
    : type_(type) {}
  Piece::Piece(Type const type, unsigned int posx, unsigned int posy)
    : position_({static_cast<int>(posx), static_cast<int>(posy)}),
    type_(type) {}
  Piece::Piece(QDataStream &in) {
    in >> type_ >> position_;
  }
  void Piece::demote() {
    if(promoted())
      type_ = ShogiServer::promotion(type_);
  }
  bool Piece::deployed() const {
    return (x(position_) >= 0) && (y(position_) >= 0);
  }
  bool Piece::drop(int const x, int const y) {
    if(droppable()) {
      position_ = {x,y};
      return true;
    } else {
      return false;
    }
  }
  bool Piece::droppable() const {
    //!\todo deployed should be enough: check. convert to assert?
    return (! deployed()) && (! promoted());
  }
  bool Piece::move(Position const position) {
    if(! deployed()) // Cannot move if not on the board.
      return false;

    Position relative{x(position) - x(position_), y(position) - y(position_)}; // This is what matters to determine whether the move is legal
    if(relative == Position({0,0})) //Check that the move is an actual move; although what to return is this case is doubtful
      return false;

    for(auto &move : moves()){
      if(move.second) {
        for(int idx = 1; idx <= std::max(abs(x(relative)), abs(y(relative))); ++idx) { // Brute force; there are more intelligent methods, but this should still be fast enough
          if((x(move.first) * idx == x(relative)) && (y(move.first) * idx == y(relative))) {
            position_ = position;
            return true;
          }
        }
      } else {
        if(move.first == relative) {
          position_ = position;
          return true;
        }
      }
    }
    return false; // If we got here, it was not a valid move
  }
  MovesList Piece::moves() const {
    return ShogiServer::moves(type_);
  }
  void Piece::promote() {
    if((! promoted()) && (promotion(type_) != Type::none))
      type_ = promotion(type_);
  }
  bool Piece::promoted() const {
    return ShogiServer::promoted(type_);
  }
  void Piece::remove() {
    position_ = {-1, -1};
    demote();
  }

  bool Piece::displace(Position const position) {
    if(! deployed()) // Cannot move if not on the board.
      return false;

    position_ = position;
          return true;
  }
}
