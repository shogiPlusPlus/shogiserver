/***************************************************************************
 * Copyright (C) 2021 Francesco Florian, Élise Le Mélédo
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "board.h"

#include "piece.h"

namespace ShogiServer {
  const Board::DropList Board::emptyDropList{};

  bool Board::canDrop(PlayerId const player, Piece::Type const piece) const {
    return (dropLists_[static_cast<size_t>(player)].find(piece) != dropLists_[static_cast<size_t>(player)].end());
  }
  bool Board::doDrop(PlayerId const player, Piece::Type const piece) {
    return ! dropLists_[static_cast<size_t>(player)].extract(piece).empty();
  }
  void Board::addToDropList(PlayerId const player, Piece::Type const piece) {
    (void) dropLists_[static_cast<size_t>(player)].insert(piece);
  }
}
