/***************************************************************************
 * Copyright (C) 2021 Francesco Florian
 * This file is part of shogiserver.
 *
 * shogiserver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shogiserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the Lesser GNU General Public License
 * along with shogiserver.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef LOGGER_H
#define LOGGER_H

#include <string>

#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_DEFAULT "\x1b[0m"

namespace ShogiServer {
  /**
   * Helper functions to display log messages based on the severity.
   *
   * Unsetting the preprocessor variable _Logging makes all the logging functions inline empty functions, which should then be optimized away at compile time (unless inlining is disabled).
   */
  namespace Logger {
    enum class Color {
      blue,
      cyan,
      green,
      magenta,
      red,
      yellow,
      none
    };

    /**
     * Possible log levels.
     */
    enum class NormalLevel {
      none,
      developer,
      debugData,
      ioBasic,
      programFlow,
      dataOperation,
      max
    };
    /**
     * Possible warning levels.
     */
    enum class WarnLevel {
      none,
      fatal,
      error,
      warning,
      note,
      max
    };

    /**
     * Collections of the log devices.
     * \todo stub.
     */
    class LogDevice {
    public:
      LogDevice();
      ~LogDevice();
      void addDevice() const;
      void write(std::string const message) const;
    };
    /**
     * Log the given data if their severity level is enabled.
     */
    void log(std::string const message,                         //!< Message string.
             NormalLevel const type = NormalLevel::developer,   //!< Type of the message, to decide whether it's worth logging.
             Color const color = Color::none                    //!< Message color (useful only when using terminal output).
             );
    /**
     * Log the given data if their severity level is enabled.
     */
    void warn(std::string const message,                        //!< Message string.
              WarnLevel const type = WarnLevel::warning,        //!< Type of the message, to decide whether it's worth logging.
              Color const color = Color::none                   //!< Message color (useful only when using terminal output).
              );
    /**
     * Get the current severity level required for actual logging.
     * @return Actual severity level set.
     */
    NormalLevel logLevel();
    /**
     * Set the severity level required for actual logging.
     * @return Actual severity level set.
     */
    NormalLevel logLevel(NormalLevel const severity     //!< Target level of the messages to log: any message whose type is at least as important is logged.
                   );
    /**
     * Get the current severity level required for actual logging.
     * @return Actual severity level set.
     */
    WarnLevel warnLevel();
    /**
     * Set the severity level required for actual logging.
     * @return Actual severity level set.
     */
    WarnLevel warnLevel(WarnLevel const severity //!< Target level of the messages to log: any message whose type is at least as important is logged.
                   );
  }
}


#endif /* LOGGER_H */
